package pl.codezone.spring.cloud.service.book.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.codezone.spring.cloud.service.book.model.Book;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksController {

    private List<Book> bookList = Arrays.asList(
        new Book(1L, "Symfonia C++", "Jerzy Grębosz"),
        new Book(2L, "Thinking in Java", "Bruce Eckel")
    );

    @GetMapping("")
    public List<Book> findAllBooks() {
        return bookList;
    }

    @GetMapping("/{bookId}")
    public Book findBook(@PathVariable Long bookId) {
        return bookList.stream().filter(book -> book.getId().equals(bookId)).findFirst().orElse(new Book());
    }

}